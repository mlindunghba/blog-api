@extends('layouts/main')

@section('title', 'New Article')

@section('content')
<h2 class="m-5">New Article</h2>
<form method="post">
@csrf
    <div class="mb-3">
        <label for="frm-title" class="form-label">Title</label>
        <input type="text" class="form-control" name="frm-title" id="frm-title" placeholder="Article Title">
    </div>
    <div class="mb-3">
    <label for="frm-content" class="form-label">Content</label>
    <textarea name="frm-content" class="form-control" id="frm-content" rows="3"></textarea>
    </div>
    <div class="mb-3">
        <button type="submit" class="btn btn-primary form-control">Submit</button>
    </div>
</form>
@endsection

@section('pagescript')
    @parent
    <script type="text/javascript">
        window.addEventListener('DOMContentLoaded', (event) => {
            tinymce.init({
                selector: 'textarea#frm-content',
                content_css: false,
                skin: false
            });
        });
    </script>
@endsection