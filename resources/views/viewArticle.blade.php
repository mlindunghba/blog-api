@extends('layouts/main')

@section('title', $data->title)

@section('content')
<br>
<div class="jumbotron bg-light">
  <div class="container-fluid">
    <h1 class="display-4">{{$data->title}}</h1>
    <p class="lead">{{Carbon::parse($data->created_at)->format('l, d M Y H:i:s')}} | Author : {{$author->name}}</p>
    <hr class="my-4">
    <p>{!! $data->content !!}</p><br>
    <p class="lead">
      {{--<a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>--}}
    </p>
  </div>
</div>

<h4>Komentar :</h4>

<ul class="list-group">
@foreach($comments as $comment)
  <li class="list-group-item"><b>{{$comment->author}} <br> <small class="text-secondary">{{Carbon::parse($comment->created_at)->format('d M Y H:i:s')}}</small> </b> <br>{!!$comment->content!!}</li>
@endforeach
</ul>

<div class="card">
  <div class="card-body bg-dark">
  <div class="container-fluid">
        <form action="{{route('articles.comment', $data->id)}}" method="post">
        @csrf
        <div class="input-group mb-3">
          <span class="input-group-text">{{Auth::user()->name}} :</span>
          <input type="text" class="form-control" name="comment" aria-label="Masukkan Komentar" placeholder="Masukkan Komentar" require>
          <span class="input-group-text"><button type="submit" class="btn btn-primary">Comment</button></span>
        </div>
        </form>
    </div>
  </div>
</div>
@endsection