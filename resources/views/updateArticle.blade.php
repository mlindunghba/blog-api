@extends('layouts.main')

@section('title', '- '.$data->title.' | Update Articles')

@section('content')
    @if(session('success'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <span>{!! session('success') !!}</span>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    <h2 class="m-5"><a href="{{url('/')}}" class="text-secondary">Kembali</a> | Update data {{ $data->title }}</h2>
    <form action="{{route('article.update', $data->id)}}" method="POST">
        @method('PUT')
        @csrf

        {{--<div class="mb-3">
            <input type="hidden" class="form-control" id="idf" name="idf" value="{{ $data->id }}" placeholder="input here...">
        </div>--}}
        <div class="mb-3">
            <small>Author : {{ $author->name }} at {{ Carbon::parse($data->created_at)->format('d M Y H:i:s') }}</small>
        </div>
        <div class="mb-3">
            <label for="frm-title" class="form-label">Title</label>
            <input type="text" class="form-control" id="frm-title" name="frm-title" value="{{ $data->title }}" placeholder="input here...">
        </div>
        <div class="mb-3">
            <label for="frm-content" class="form-label">Content</label>
            <textarea class="form-control" id="frm-content" name="frm-content" rows="3" placeholder="Type text in here...">{{ $data->content }}</textarea>
        </div>
        <div class="mb-3">
            <button type="submit" class="btn btn-warning text-white form-control">Update</button>
        </div>
    </form>
@endsection

@section('pagescript')
    @parent
    <script type="text/javascript">
        window.addEventListener('DOMContentLoaded', (event) => {
            tinymce.init({
                selector: 'textarea#frm-content',
                content_css: false,
                skin: false
            });
        });
    </script>
@endsection