<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <title>WebBlog @yield('title', 'Main')</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a href="#" class="navbar-brand">Webblog</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" arial-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a href="{{url('/')}}" class="nav-link" aria-current="page">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('/articles/new')}}" class="nav-link">New Article</a>
                    </li>
                </ul>
            </div>
            @if(Auth::check())
            <div class="d-flex text-light">
                <small class="m-3 ">{{ Auth::user()->name }}</small>
                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                    <img src="{{asset('img/user-icon.jpg')}}" alt="" width="45" height="45" class="rounded d-inline-block align-text-top" style="border-radius: 50px !important;">
                </a>

                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuLink">
                    <li class="dropdown-item"><p class="mb-0">
                    {{ Auth::user()->email }} <br> <small>{{ Auth::user()->name }}</small>
                    </p></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="{{ route('logout') }}">Logout</a></li>
                </ul>
            </div>
            @endif
        </div>
    </nav>

    <div class="container">
        @yield('content')
    </div>
<br>
    <footer class="text-center text-white bg-dark">
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
       
        <a class="text-white" href="{{url('/')}}">Mochamad Lindung Hasti Budi Aldany - 1942455</a>
        <p>Application Programming Interface <br>Web Blog <br>{{Carbon::now()->format('Y')}}</p>
        
        
    </div>
    <!-- Copyright -->
    </footer>

    @section('pagescript')
    <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
    @show
</body>
</html>