<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WeblogSite | @yield('title')</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>
<br>
<br>
    <div class="container">
        @yield('content')
    </div>
    <footer class="text-center text-white fixed-bottom bg-dark">
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
       
        <a class="text-white" href="{{url('/')}}">Mochamad Lindung Hasti Budi Aldany - 1942455</a>
        <p>Application Programming Interface <br>Web Blog <br>{{Carbon::now()->format('Y')}}</p>
        
        
    </div>
    <!-- Copyright -->
    </footer>
    @section('pagescript')
    <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
    @show
</body>
</html>