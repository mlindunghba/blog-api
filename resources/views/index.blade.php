@extends('layouts/main')

@section('content')
<br>
    @if(session('success'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <span>{!! session('success') !!}</span>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <div class="row row-cols-1 row-cols-md-3 g-4">
    @foreach($data as $record)
        <div class="col">
            <div class="card h-100">
                <div class="card-header">
                    <div class="container-fluid">
                    <a href="{{url('/articles', $record->id)}}" class="text-dark" style="text-decoration:none;" >
                        <h2>{{$record->title}}</h2>
                    </a>
                    </div>
                </div>
                <div class="card-body">
                @if(Auth::check())
                <small class="text-secondary"><a href="{{route('article.update', $record->id)}}" class="text-secondary">Edit</a> | <a href="{{route('articles.delete', $record->id)}}" class="text-secondary">Hapus</a></small>
                @endif
                    <h5 class="card-title"></h5>
                    <p class="card-text">{!! Str::limit($record->content, 400) !!} <a href="{{url('/articles', $record->id)}}" style="text-decoration: none;">Baca Selengkapnya</a> </p>
                </div>
                <div class="card-footer text-center">
                    <small class="text-muted">{{ Carbon::parse($record->created_at)->diffForHumans() }} | {{ Carbon::parse($record->created_at)->format('d M Y H:i:s') }}</small>
                </div>
            </div>
        </div>
        @endforeach
    </div>
@endsection
