<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth
Route::get('login', [AuthController::class, 'indexLogin'])->name('login.view');
Route::post('login', [AuthController::class, 'login'])->name('login'); 
Route::get('register', [AuthController::class, 'indexRegister'])->name('register.view');
Route::post('register', [AuthController::class, 'register'])->name('register'); 
Route::get('logout', [AuthController::class, 'logout'])->name('logout');

Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard'); 

Route::get('/', [SiteController::class, 'index']);
Route::redirect('/articles', '/');	
Route::match(['get', 'post'], '/articles/new', [SiteController::class, 'newArticles']);
Route::match(['get', 'put'], '/articles/update/{id}', [SiteController::class, 'updateArticles'])->name('article.update');
Route::get('articles/{id}', [SiteController::class, 'getArticles']);
Route::get('articles/delete/{id}', [SiteController::class, 'deleteArticles'])->name('articles.delete');

Route::post('comment/{id_artikel}/add', [SiteController::class, 'postComment'])->name('articles.comment');