<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    public function index()
    {
        $has = Session::has('name') && Session::has('email');

        if ($has) {
            return redirect('/');
        }

        return redirect('login');
    }
}
