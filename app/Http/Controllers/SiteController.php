<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;    
use GuzzleHttp\Client;

use Carbon\Carbon;

class SiteController extends Controller
{
    const API_BASE = 'https://blog-api.stmik-amikbandung.ac.id/api/v2/blog/_table/';
    const API_KEY = 'ef9187e17dce5e8a5da6a5d16ba760b75cadd53d19601a16713e5b7c4f683e1b';
    private $apiClient;
    public function __construct()
    {
        $this->middleware('auth');
        $this->apiClient = new Client([
            'base_uri'  => self::API_BASE,
            'headers'   => [
                'X-DreamFactory-API-Key' => self::API_KEY
            ]
        ]);

    }
    public function index()
    {
        $data = Cache::get('index', function(){
            try {
                $reqData = $this->apiClient->get('articles');
                $resource = json_decode($reqData->getBody())->resource;
                Cache::add('index', $resource);
                return $resource;
            } catch (RequestException $e) {
                return [];
            }
        });
        
        return view('index', ['data' => $data]);
    }

    public function getAuthorArticle()
    {
        $data['author'] = $this->index()->data;
        // dd($data)[1];
        return $data;
    }

    public function getArticles($id)
    {
        
        $key = "articles/{$id}";
        $data = Cache::get($key, function() use ($key){
            try {
                $reqData = $this->apiClient->get($key);
                $resource = json_decode($reqData->getBody());

                Cache::add($key, $resource);
                return $resource;
            } catch (Exception $e) {
                abort(404);
            }
        });

        $comments = $this->getComments($data->id);
        
        // $this->getComments($data->id);

        try{

            $urlGetAuthor = "authors/{$data->author}";
            $reqDataAuthor = $this->apiClient->get($urlGetAuthor);
            $resourceAuthor = json_decode($reqDataAuthor->getBody());

        }catch(Exception $e){
            // ERROR
            abort(500);
        }
        
        return view('viewArticle', ['data' => $data, 'comments' => $comments, 'author' => $resourceAuthor]);
    }

    public function newArticles(Request $request)
    {
        if ($request->isMethod('post')) {
            
            $title = $request->input('frm-title');
            $content = $request->input('frm-content');
            $author_id = Auth::user()->author_id;

            $dataModel = [
                'resource' => []
            ];

            $dataModel['resource'][] = [
                'author' => $author_id,
                'title' => $title,
                'content' => $content
            ];

            try {
                $reqData = $this->apiClient->post('articles', [
                    'json' => $dataModel
                ]);
                $apiResponse = json_decode($reqData->getBody())->resource;
                $newId = $apiResponse[0]->id;

                Cache::forget('index');

                return redirect("articles/{$newId}");
            } catch (Exception $e) {
                abort(501);
            }
        }

        return view('newArticle');
    }

    public function updateArticles(Request $request, $id)
    {
        $key = "articles/{$id}";
        Cache::forget($key);
        $data = Cache::get($key, function () use ($key) {
            try {
                $reqData = $this->apiClient->get($key);
                $resource = json_decode($reqData->getBody());
                Cache::add($key, $resource);
                return $resource;
            } catch (Exception $e) {
                abort(404);
            }
        });

        $author_id = $data->author;

        try{

            $urlGetAuthor = "authors/{$author_id}";
            $reqDataAuthor = $this->apiClient->get($urlGetAuthor);
            $resourceAuthor = json_decode($reqDataAuthor->getBody());

        }catch(Exception $e){
            // ERROR
            abort(500);
        }

        if ($request->isMethod('put')) {
            
            $title = $request->input('frm-title');
            $content = $request->input('frm-content');

            $dataModel = [
                'resource' => []
            ];
            $dataModel['resource'][] = [
                'id' => $id,
                'title' => $title,
                'content' => $content
            ];

            $reqData = $this->apiClient->patch("articles", [
                'json' => $dataModel
            ]);       

            $resource = $dataModel['resource'][0];
            
            Cache::add($key, $resource);
            Cache::forget('index');
            Cache::forget($key);

            return redirect()->back()->withSuccess('Data Berhasil diubah !');
        }

        return view('updateArticle', ['data' => $data, 'author' => $resourceAuthor]);
    }

    public function deleteArticles($id)
    {
    
        $dataModel = [
            'resource' => []
        ];

        $dataModel['resource'][] = [
            'id'=>$id
        ];
        $key = "articles/{$id}";
        try{
            $reqData = $this->apiClient->delete("articles/{$id}", [
                'json' => $dataModel
            ]);

            Cache::forget('index');
            Cache::forget($key);
            
            return redirect('/')->withSuccess('Data Berhasil dihapus');
        } catch(Exception $e){
            abort(501);
        }
    }
    
    public function comments()
    {
        
            try {
                $reqData = $this->apiClient->get('comments');
                $resource = json_decode($reqData->getBody())->resource;
                
                return $resource;
            } catch (RequestException $e) {
                return [];
            }
        
    }

    public function getComments(int $id_artikel)
    {
        $comments = $this->comments();
        $hasil = [];
        foreach($comments as $c){
            if($c->article === $id_artikel){
                $hasil[] = $c;
            }
        }
        
        return $hasil;
    }

    public function postComment(Request $request, $id_artikel)
    {
        
        if ($request->isMethod('post')) {
            
            $comment = $request->input('comment');
            $author = Auth::user()->name;

            $dataModel = [
                'resource' => []
            ];

            $dataModel['resource'][] = [
                'author' => $author,
                'article' => $id_artikel,
                'content' => $comment,
                'created_at' => Carbon::now()
            ];

            try {
                $reqData = $this->apiClient->post('comments', [
                    'json' => $dataModel
                ]);
                $apiResponse = json_decode($reqData->getBody())->resource;
                $newId = $apiResponse[0]->id;

                Cache::forget('index');

                return redirect()->back();
            } catch (Exception $e) {
                abort(501);
            }
        }
    }
}

