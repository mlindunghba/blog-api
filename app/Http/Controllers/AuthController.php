<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

use Hash;
use Session;
use Exception;
use Carbon\Carbon;
use GuzzleHttp\Client;

use App\Models\User;

class AuthController extends Controller
{

    const API_BASE = 'https://blog-api.stmik-amikbandung.ac.id/api/v2/blog/_table/';
    const API_KEY = 'ef9187e17dce5e8a5da6a5d16ba760b75cadd53d19601a16713e5b7c4f683e1b';
    private $apiClient;

    public function __construct()
    {
        $this->apiClient = new Client([
            'base_uri' => self::API_BASE,
            'headers' => [
                'X-DreamFactory-API-Key' => self::API_KEY
            ]
        ]);
    } 

    public function indexLogin()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);
   
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect('/')->with('success', 'Hallo '.Auth::user()->name." !");
        }
  
        return redirect("login")->withErrors('Email atau Password salah !');
    }

    public function indexRegister()
    {
        return view('auth.register');
    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);
           
        $data = $request->all();
        $check = $this->create($data);
         
        return redirect(route('login'))->with('success', $request->input('email')." Berhasil terdaftar");
    }

    public function create(array $data)
    {
            $name = $data['name'];
            $email = $data['email'];

            $dataModel = [
                'resource' => []
            ];

            $dataModel['resource'][] = [
                'name' => $name,
                'email' => $email,
            ];

            try {
                $reqData = $this->apiClient->post('authors', [
                    'json' => $dataModel
                ]);
                $apiResponse = json_decode($reqData->getBody())->resource;

                Cache::forget('index');

                $author_id = json_decode($apiResponse[0]->id);

            } catch (Exception $e) {
                return abort(500);
            }

            return User::create([
                'author_id' => $author_id,
                'name' => $name,
                'email' => $email,
                'password' => Hash::make($data['password'])
              ]);
    } 

    public function logout()
    {
        Session::flush();
        Auth::logout();
  
        return Redirect('login');
    }

}
